import math
from math import sqrt
import random
import igraph as ig
import community
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from networkx.algorithms.community.quality import performance
from networkx.algorithms.community import modularity
from sklearn.cluster import KMeans
from networkx.algorithms.community.quality import coverage
from networkx.algorithms import community as cmt
import pandas as pd


class Node:

    def __init__(self, d, ind):
        self.x = random.random()*100
        self.y = random.random()*100
        self.d = d
        self.ind = ind


# n = 80
# G = nx.barbell_graph(10, 0)
# G = nx.read_weighted_edgelist('network.dat', create_using=nx.Graph(), nodetype=int)
# G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
# G = nx.read_weighted_edgelist('', create_using=nx.Graph(), nodetype=int)

def import_nx_network(net):
    graph = ig.Graph(n=net.number_of_nodes(), directed=False)
    graph.add_edges(net.edges())

    return graph


def LFR_graph(N, mu1, mu2, tau, min_degree, min_community):
    net = cmt.LFR_benchmark_graph(N, mu1, mu2, tau, average_degree=min_degree, min_community=min_community, seed=10)
    graph = import_nx_network(net)

    return graph, net


n = 100
tau1 = 3
tau2 = 1.5
mu = 0.08
min_deg = 20
min_com = 20
g, G = LFR_graph(n, tau1, tau2, mu, min_deg, min_com)
# n = 80
n = G.number_of_nodes()
pos = nx.spring_layout(G)
nx.draw(G, pos)
plt.draw()
plt.show()
b = nx.adjacency_matrix(G).toarray()
nodes = list(G.nodes)


def APD(A, N):
    Z = np.matmul(A, A)
    B = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if (A[i][j] == 1 or Z[i][j] > 0) and i != j:
                B[i][j] = 1
    if all(B[i][j] for i in range(n) for j in range(n) if i != j):
        D = 2 * B - A
        return D
    T = APD(B, N)
    X = np.matmul(T, A)
    degree = [sum(A[i][j] for j in range(n)) for i in range(n)]
    D = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if X[i][j] >= T[i][j] * degree[j]:
                D[i][j] = 2 * T[i][j]
            else:
                D[i][j] = 2 * T[i][j] - 1
    return D


# sp_list = pd.read_csv('sp_list.csv').as_matrix()
d = list(G.degree)

sp_list = APD(b, n)
s = time.time()
node_list = [Node(d[i][1], i) for i in range(0, n)]
sp_list_new = sp_list.copy()
sp_list_new[sp_list_new > sp_list_new.max()/2] = -10000000
sp_list_new[sp_list_new == 0] = -1
a = [list(G.degree)[i][1] for i in range(len(list(G.degree)))]
P = np.zeros((n, n))
for i in range(len(a)):
    P[:][i] = b[:][i] * (1 / a[i])

p2 = np.matmul(P, P)
p4 = np.matmul(p2, p2)
condition_list_new = (1 / (sp_list_new * [d[i][1] for i in range(0, n)])).transpose()
condition_list_new[condition_list_new >= 0] = -1000000
# condition_list_new = np.triu(condition_list_new, 0)
# condition_list_new = np.maximum(condition_list_new, condition_list_new.transpose())

# p4 = np.matmul(p2, p2)

node_list.sort(key=lambda x: x.d)
chosen_nodes = np.zeros(n)
for node in node_list:
    chosen_node = np.argwhere(condition_list_new[node.ind][:] == np.amax(condition_list_new[node.ind][:]))
    if chosen_node.__len__() == 1:
        chosen_nodes[node.ind] = chosen_node[0]
    else:
        min_p = 0
        chosen = 0
        for c in chosen_node:
            if p2[node.ind][c] > min_p:
                min_p = p2[node.ind][c]
                chosen = c
        chosen_nodes[node.ind] = chosen

node_list.sort(key=lambda x: x.ind)
i = 0
while i < len(node_list):
    node_list[i].x = node_list[int(chosen_nodes[i])].x
    node_list[i].y = node_list[int(chosen_nodes[i])].y
    i += 1
e = time.time()

x = []
y = []
for item in node_list:
    x.append(item.x)
    y.append(item.y)

# types = [i for i in range(1, n)]
# for i, type in enumerate(types):
#     z = x[i]
#     t = y[i]
#     plt.scatter(z, t, marker='x', color='red')
#     plt.text(z+0.3, t+0.3, type, fontsize=9)
# plt.show()

X = []
for item in x:
    X.append((item, y[x.index(item)]))

y_pred = KMeans(n_clusters=n).fit_predict(X)
nx.draw(G, pos, node_color=y_pred)
plt.draw()
plt.show()

d = [[] for x in range(len([item for item in set(y_pred)]))]
clusters = [item for item in set(y_pred)]
for item in clusters:
    for node in range(len(y_pred)):
        if y_pred[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
print('my algorithm', performance(G, sets), coverage(G, sets), modularity(G, sets))
print('my algorithm Time', e - s, '\n')


start = time.time()
partition = community.best_partition(G)
end = time.time()
y_kl = []
for item in partition:
    y_kl.append(partition[item])

d = [[] for x in range(len([item for item in set(y_kl)]))]
clusters = [item for item in set(y_kl)]

for item in clusters:
    for node in range(len(y_kl)):
        if y_kl[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
#
print('Kernighan–Lin algorithm', performance(G, sets), coverage(G, sets), modularity(G, sets))
print('Kernighan–Lin algorithm Time', end - start, '\n')
#

st = time.time()
greedy_modularity_communities = nx.algorithms.community.greedy_modularity_communities(G)
en = time.time()
print('greedy_modularity_community algorithm', performance(G, greedy_modularity_communities), coverage(G, greedy_modularity_communities), modularity(G, greedy_modularity_communities))
print('greedy_modularity_community algorithm Time', en - st, '\n')

