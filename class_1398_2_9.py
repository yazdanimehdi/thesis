import itertools

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm

df = pd.read_csv('bank.csv')


def yes_to_one(y):
    if y == 'yes':
        return 1
    elif y == 'no':
        return 0
    else:
        return None


df['y'] = df['y'].apply(yes_to_one)
# pd.pivot_table(df, values='y', index=['marital', 'job'], columns=['education', 'housing'], aggfunc=np.sum)
# pd.crosstab(df["job"],df["y"],margins=True)
# df['job'].unique()
# i = 0
# index_list = df.dtypes.index.tolist()
# for item in df.dtypes:
#     if str(item) == 'float64':
#         plt.cla()
#         plt.clf()
#         pd.DataFrame(df[index_list[i]]).hist()
#         plt.show()
#     i += 1
#
# grades = list(df['job'].unique())
#
# totals_grades = list()
# for item in grades:
#     totals_grades.append(df.loc[df['job'] == item]['job'].count())
#
# plt.pie(totals_grades, labels=grades)
# plt.show()
# plt.clf()
# plt.cla()
X = df['age'].values.reshape(-1, 1)
y = df['balance'].values.reshape(-1,1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
lr = LinearRegression()
lr.fit(X_train, y_train)
print(lr.intercept_)
print(lr.coef_)
y_pred = lr.predict(X_test)
df_reg = pd.DataFrame({'Actual': y_test.flatten(), 'Predicted': y_pred.flatten()})
df1 = df.head(25)
df1.plot(kind='bar',figsize=(16,10))
plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
plt.show()
plt.scatter(X_test, y_test,  color='gray')
plt.plot(X_test, y_pred, color='red', linewidth=2)
plt.show()
print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()


tree = DecisionTreeClassifier()
random_forrest = RandomForestClassifier()
logistic_reg = LogisticRegression()
y_pred = tree.predict(X_test)
cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)