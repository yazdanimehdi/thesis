import time
import community
import igraph as ig
from networkx.algorithms import community as cmt
import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from networkx.algorithms.community.quality import performance
import random
import ast
from sklearn.metrics.cluster import normalized_mutual_info_score


def import_nx_network(net):
    graph = ig.Graph(n=net.number_of_nodes(), directed=False)
    graph.add_edges(net.edges())

    return graph


def LFR_graph(N, mu1, mu2, tau, min_degree, min_community):
    net = cmt.LFR_benchmark_graph(N, mu1, mu2, tau, average_degree=min_degree, min_community=min_community, seed=10)
    graph = import_nx_network(net)

    return graph, net


n = 100
tau1 = 3
tau2 = 1.5
mu = 0.09
min_deg = 10
min_com = 30
# G = nx.read_weighted_edgelist('1K25.nse', create_using=nx.Graph(), nodetype=int)
# g = ig.read('1K25.nse')
g, G = LFR_graph(n, tau1, tau2, mu, min_deg, min_com)
# #
# G = nx.barbell_graph(4,0)
# G = nx.read_edgelist('network.dat', create_using=nx.Graph(), nodetype=int)
n = G.number_of_nodes()
m = G.number_of_edges()
pos = nx.spring_layout(G)

# print(nx.clustering(a))
nx.draw(G, pos)
plt.draw()
plt.show()
b = nx.adjacency_matrix(G).toarray()
a = [list(G.degree)[i][1] for i in range(len(list(G.degree)))]
P = np.zeros((n, n))
for i in range(len(a)):
    P[:][i] = b[:][i] * (1 / a[i])
g = ig.Graph.Adjacency(b.tolist())
p2 = np.matmul(P, P)
p4 = np.matmul(p2, p2)
p8 = np.matmul(p4, p4)
p16 = np.matmul(p8, p8)
p32 = np.matmul(p16, p16)
p64 = np.matmul(p32, p32)
p128= np.matmul(p64, p64)
p33 = np.matmul(p32, P)

# print(nx.shortest_path(a, source=0, target=80))
sp_list = np.zeros([n, n])
nodes = list(G.nodes)

start_node = 0
a = [list(G.degree)[i][1] for i in range(len(list(G.degree)))]
# P = p16
clusters = [[i] for i in nodes]
node_list = nodes.copy()
k = node_list[2]
now_node = k
state = 0
stopping_cond = 1
cluster_0 = []
k = 0
for it in range(0, 10000):
    node_now = k
    state = 0
    stopping_cond = 1
    big = []
    clusters[k] = [k]
    flag = False
    q = 0
    while state <= stopping_cond:
        choices = np.argwhere(P[node_now] > 0)
        # for item in choices:
        #     if item in clusters[k]:
        #         choices = np.array(np.delete(choices, np.argwhere(choices == item), axis=0))
        weights = [P[node_now][i[0]] for i in choices]
        if choices.__len__() > 1:
            node_now = random.choices(choices, weights)[0][0]
        elif choices.__len__() == 1:
            node_now = choices[0][0]
        else:
            break
        if node_now not in clusters[k]:
            clusters[k].append(node_now)

        inner = 0
        for t in clusters[k]:
            for node in clusters[k]:
                inner += b[node][t]

        inner = []
        outer = []
        for t in clusters[k]:
            x = 0
            y = 0
            for node in clusters[k]:
                x += b[node][t]
            y = a[t] - x
            outer.append(y)
            inner.append(x)

        alphai = []
        i = 0
        for item in inner:
            alphai.append(item / (1 + outer[i]))
            i += 1

        sum_alpha = np.array(alphai).sum()

        agi = []
        for item in clusters[k]:
            c = []
            for node in clusters[k]:
                c.append(b[item][node])
            agi.append(c)

        dgci = []
        for item in clusters[k]:
            c = 0
            for node in clusters[k]:
                c += b[item][node]
            dgci.append(c)
        dgci = np.array(dgci) * np.eye(len(dgci))
        dgCi = []
        for item in clusters[k]:
            dgCi.append(a[item])
        dgCi = np.array(dgCi) * np.eye(len(dgCi))
        sigma = np.matmul(dgCi.transpose(), np.array(agi))
        s = np.matmul((dgci + np.eye(len(dgci))).transpose(), np.array(agi) + np.eye(len(dgci)))
        sigmae = np.linalg.eigvals(sigma)
        sigmae.sort()
        sigmaf = abs(sigmae[-1])
        se = np.linalg.eigvals(s)
        se.sort()
        try:
            sef = 1 - abs(se[-2])
        except:
            sef = 1 - abs(se[-1])
        state2 = sum_alpha * sef * sigmaf

        # if state != 0:
        #     if state2 < state:
        #         print('yess')
        #         flag = True
        #         break
        #     else:
        #         state = state2
        # else:
        #     state = state2
        state2 = state

        bigger = []
        performances = []
        for item in np.argwhere(P[node_now] > 0):
            next_step = clusters[k].copy()
            if item[0] not in next_step:
                next_step.append(item[0])
                inner = []
                outer = []
                for t in next_step:
                    x = 0
                    y = 0
                    for node in next_step:
                        x += b[node][t]
                    y = a[t] - x
                    outer.append(y)
                    inner.append(x)

                alphai = []
                i = 0
                for q in inner:
                    alphai.append(q / (1 + outer[i]))
                    i += 1

                sum_alpha = np.array(alphai).sum()

                agi = []
                for q in next_step:
                    c = []
                    for node in next_step:
                        c.append(b[q][node])
                    agi.append(c)

                dgci = []
                for q in next_step:
                    c = 0
                    for node in next_step:
                        c += b[q][node]
                    dgci.append(c)
                dgci = np.array(dgci) * np.eye(len(dgci))
                dgCi = []
                for q in next_step:
                    dgCi.append(a[q])
                dgCi = np.array(dgCi) * np.eye(len(dgCi))
                sigma = np.matmul(dgCi.transpose(), np.array(agi))
                s = np.matmul((dgci + np.eye(len(dgci))).transpose(), np.array(agi) + np.eye(len(dgci)))
                sigmae = np.linalg.eigvals(sigma)
                sigmae.sort()
                sigmaf = abs(sigmae[-1])
                se = np.linalg.eigvals(s)
                se.sort()
                sef = 1 - abs(se[-2])
                performance_next = sum_alpha * sef * sigmaf

            else:
                performance_next = state

            bigger.append((item[0], performance_next))
        big = []
        for item in bigger:
            if item[1] >= state:
                big.append(item)

        allall = np.argwhere(P[node_now] > 0).__len__()
        p = big.__len__()/allall
        ej = 0
        for item in bigger:
            ej += P[node_now][item[0]] * item[1]

        # if p == 1:
        #     stopping_cond = np.inf
        # else:
        stopping_cond = ej
        print('.........')
        print(state)
        print(stopping_cond)
        print(clusters[k])
        print('..........')
    # if flag is False:
    cluster_0.append(clusters[k])
#
cluster_0_final = []
bb = dict()
for item in cluster_0:
    if str(item) in bb:
        bb[str(item)] += 1
    else:
        bb[str(item)] = 1
sorted_x = sorted(bb.items(), key=lambda kv: -kv[1])
ordered_list = []
for item in sorted_x:
    if item[1] >= 0:
        x = ast.literal_eval(item[0])
        ordered_list.append(x)
node_dict = dict()
for item in ordered_list:
    for node in item:
        if node in node_dict:
            node_dict[node] += 1
        else:
            node_dict[node] = 1
sorted_node = sorted(node_dict.items(), key=lambda kv: -kv[1])

for item in ordered_list:
    cluster_0_final += item

cluster_0_final = set(cluster_0_final)

my_list = []
for item in sorted_node:
    if item[1] >= 6:
        my_list.append(item[0])
a_m = np.zeros(n)
for o in my_list:
    a_m[o] = 1
# #
a_t = np.zeros(n)
for o in G.nodes[0]['community']:
    a_t[o] = 1
print('my_alg_nmi', ig.compare_communities(ig.Clustering(a_m), ig.Clustering(a_t), method='nmi'))

a_m = np.zeros(n)
i = 0
for o in g.community_walktrap().as_clustering().membership:
    if o == 0:
        a_m[i] = 1
    i += 1
print('walk_trap_nmi', ig.compare_communities(ig.Clustering(a_m), ig.Clustering(a_t), method='nmi'))

a_m = np.zeros(n)
i = 0
for o in g.community_infomap().membership:
    if o == 0:
        a_m[i] = 1
    i += 1
print('info_map_nmi', ig.compare_communities(ig.Clustering(a_m), ig.Clustering(a_t), method='nmi'))
# a_m = np.zeros(n)
# for o in g.community_walktrap().as_clustering().membership:
#     if o == 1:
#         a_m[o] = 1
# print('walk_trap_nmi', ig.compare_communities(ig.Clustering(a_m), ig.Clustering(a_t), method='nmi'))
#

list_with_score = []
for item in ordered_list:
    outer = 0
    for c in item:
        for node in nodes:
            if node not in item:
                outer += b[c][node]
    len_cluster = item.__len__()
    conductance = outer / min(len_cluster, n - len_cluster)
    # # sets = []
    # # for n in nodes:
    # #     a = []
    # #     if n not in item:
    # #         a.append(n)
    # #         sets.append(a)
    # # sets.append(set(item))
    # inner = 0
    # for t in clusters[k]:
    #     for node in clusters[k]:
    #         inner += b[node][t]
    # #
    # inner = inner / 2
    # density = inner/(len_cluster*(len_cluster-1))
    # state = performance(G, sets)
    # a_m = np.zeros(n)
    # for o in item:
    #     a_m[o] = 1
    perf = -conductance
    # pout = 0
    # pin = 0
    # for inode in item:
    #     for node in nodes:
    #         if node not in item:
    #             pout += p128[inode][node]
    #         else:
    #             pin += p128[inode][node]
    # perf = (pin / pout)
    list_with_score.append((item, perf))

list_with_score.sort(key=lambda x: -x[1])
clus = []
i = 0

for item in list_with_score:
    if len(clus) <= 33:
        for node in item[0]:
            if node not in clus:
                clus.append(node)


a_m = np.zeros(100)
for o in clus:
    a_m[o] = 1

print('my_alg_nmi_2', ig.compare_communities(ig.Clustering(a_m), ig.Clustering(a_t), method='nmi'))
scores = []
for item in list_with_score:
    i = 0
    for node in item[0]:
        if node in G.nodes[0]['community']:
            i += 1
    scores.append(i/len(item[0]))

counter = 0
for item in scores:
    if item > 0.9:
        counter += 1

print(counter/len(scores))