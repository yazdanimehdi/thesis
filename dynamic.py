import math
from math import sqrt
import random

import community
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from networkx.algorithms.community.quality import performance
from sklearn.cluster import KMeans
import pandas as pd


class Node:

    def __init__(self, d, ind):
        self.x = random.random()*100
        self.y = random.random()*100
        self.d = d
        self.ind = ind
        # self.yes = True


# G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
# n = 80

G = nx.barbell_graph(3,0)
n = G.number_of_nodes()
m = G.number_of_edges()
pos = nx.spring_layout(G)
# print(nx.clustering(a))
nx.draw(G, pos)
plt.draw()
plt.show()
b = nx.adjacency_matrix(G).toarray()
# print(nx.shortest_path(a, source=0, target=80))
sp_list = np.zeros([n, n])
nodes = list(G.nodes)


for node_i in range(0, n):
    for node_j in range(node_i, n):
        try:
            sp_list[node_i, node_j] = len(nx.shortest_path(G, source=nodes[node_i], target=nodes[node_j])) - 1
        except nx.exception.NetworkXNoPath:
            sp_list[node_i, node_j] = -1

sp_list = np.maximum(sp_list, sp_list.transpose())
# sp_list = pd.read_csv('sp_list.csv').as_matrix()
sp_list = sp_list / sp_list.max()
d = list(G.degree)
s = time.time()
node_list = [Node(d[i][1], i) for i in range(0, n)]
a = [list(G.degree)[i][1] for i in range(len(list(G.degree)))]
P = np.zeros((n, n))
for i in range(len(a)):
    P[:][i] = b[:][i] * (1 / a[i])

p2 = np.matmul(P, P)
e = time.time()
# x = []
# y = []
# for item in node_list:
#     x.append(item.x)
#     y.append(item.y)
#
# types = [i for i in range(1, n)]
# for i, type in enumerate(types):
#     z = x[i]
#     t = y[i]
#     plt.scatter(z, t, marker='x', color='red')
#     plt.text(z+0.3, t+0.3, type, fontsize=9)
# plt.show()
i = 0
# t = 0
condition_list = np.zeros([n, n])
while i < len(node_list):
    j = i + 1
    while j < len(node_list):
        if 0 < sp_list[i][j] < sp_list.max()/2:
            condition_value = (node_list[j].d)/((node_list[i].d)*(node_list[j].d))
        else:
            condition_value = -1
        condition_list[i][j] = condition_value

        j += 1
    i += 1
e = time.time()
condition_list = np.maximum(condition_list, condition_list.transpose())
# i = 0
node_list.sort(key=lambda x: x.d)
chosen_nodes = np.zeros(n)
for node in node_list:
    chosen_node = condition_list[node.ind][:].argmax()
    chosen_nodes[node.ind] = chosen_node

node_list.sort(key=lambda x: x.ind)
i = 0
while i < len(node_list):
    node_list[i].x = node_list[int(chosen_nodes[i])].x
    node_list[i].y = node_list[int(chosen_nodes[i])].y
    i += 1


x = []
y = []
for item in node_list:
    x.append(item.x)
    y.append(item.y)

types = [i for i in range(1, n)]
for i, type in enumerate(types):
    z = x[i]
    t = y[i]
    plt.scatter(z, t, marker='x', color='red')
    plt.text(z+0.3, t+0.3, type, fontsize=9)
plt.show()

X = []
for item in x:
    X.append((item, y[x.index(item)]))

y_pred = KMeans(n_clusters=n).fit_predict(X)
nx.draw(G, pos, node_color=y_pred)
plt.draw()
plt.show()

d = [[] for x in range(len([item for item in set(y_pred)]))]
clusters = [item for item in set(y_pred)]
for item in clusters:
    for node in range(len(y_pred)):
        if y_pred[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
print('my algorithm', performance(G, sets))
print('my algorithm Time', e - s, '\n')


start = time.time()
partition = community.best_partition(G)
end = time.time()
y_kl = []
for item in partition:
    y_kl.append(partition[item])

d = [[] for x in range(len([item for item in set(y_kl)]))]
clusters = [item for item in set(y_kl)]

for item in clusters:
    for node in range(len(y_kl)):
        if y_kl[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
#
print('Kernighan–Lin algorithm', performance(G, sets))
print('Kernighan–Lin algorithm Time', end - start, '\n')
#

print('greedy_modularity_communitie algorithm', performance(G, nx.algorithms.community.greedy_modularity_communities(G)))
st = time.time()
nx.algorithms.community.greedy_modularity_communities(G)
en = time.time()
print('greedy_modularity_communitie algorithm Time', en - st, '\n')

