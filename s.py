import math
from math import sqrt
import random

import community
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from networkx.algorithms.community.quality import performance
from sklearn.cluster import KMeans
import pandas as pd


class Node:

    def __init__(self, container, adjacency_arrey, visited):
        self.container = container
        self.adjacency_matrix = adjacency_arrey
        self.visited = visited


digekstra = []
my_algorithm = []

# n = 400
G = nx.wheel_graph(40)
# G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
n = G.number_of_nodes()
b = nx.adjacency_matrix(G).toarray()
node_list = [Node([[1, j] for j in np.where(b[i][:] > 0)[0]], b[i][:], False) for i in range(0, n)]
sp_list = np.zeros([n, n])
nodes = list(G.nodes)

# s = time.time()
# for node_i in range(0, n):
#     for node_j in range(node_i, n):
#         try:
#             sp_list[node_i, node_j] = len(nx.shortest_path(G, source=nodes[node_i], target=nodes[node_j])) - 1
#         except nx.exception.NetworkXNoPath:
#             sp_list[node_i, node_j] = -1

# sp_list = np.maximum(sp_list, sp_list.transpose())
# e = time.time()
s_m = time.time()
sp = np.zeros([n, n])
for i in range(0, n):
    minimum = {}
    for item in node_list[i].container:
        minimum[item[1]] = item[0]
        if node_list[item[1]].visited:
            for node in node_list[item[1]].container:
                if node[1] not in [k[1] for k in node_list[i].container]:
                    if node[1] != i:
                        if node[1] in minimum:
                            if minimum[node[1]] > node[0] + item[0]:
                                minimum[node[1]] = node[0] + item[0]
                        else:
                            minimum[node[1]] = node[0] + item[0]
        else:
            for node in node_list[item[1]].container:
                if node[1] != i:
                    if node[1] not in [k[1] for k in node_list[i].container]:
                        node_list[i].container.append([item[0] + node[0], node[1]])
                        if node[1] in minimum:
                            if minimum[node[1]] > item[0] + node[0]:
                                minimum[node[1]] = item[0] + node[0]
                        else:
                            minimum[node[1]] = item[0] + node[0]
    sp_list_i = []
    for item in minimum:
        sp_list_i.append([minimum[item], item])
    node_list[i].visited = True

    for item in sp_list_i:
        sp[i][item[1]] = item[0]
    node_list[i].container = sp_list_i
e_m = time.time()
# m = b.copy()
# m = m.astype('float')
# floyd_warshall Algorithm
# s_w = time.time()
# for i in range(0, n):
#     for j in range(0, n):
#         if i == j:
#             m[i][j] = 0
#         else:
#             if m[i][j] == 0:
#                 m[i][j] = np.infty
#
# for k in range(0, n):
#     for i in range(0, n):
#         for j in range(0, n):
#             newDistance = m[i][k] + m[k][j]
#             if newDistance < m[i][j]:
#                 m[i][j] = newDistance
#
# e_w = time.time()


# sp = np.maximum(sp, sp.transpose())

# Seidel's algorithm

def APD(A, N):
    Z = np.matmul(A, A)
    B = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if (A[i][j] == 1 or Z[i][j] > 0) and i != j:
                B[i][j] = 1
    if all(B[i][j] for i in range(n) for j in range(n) if i != j):
        D = 2 * B - A
        return D
    T = APD(B, N)
    X = np.matmul(T, A)
    degree = [sum(A[i][j] for j in range(n)) for i in range(n)]
    D = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if X[i][j] >= T[i][j] * degree[j]:
                D[i][j] = 2 * T[i][j]
            else:
                D[i][j] = 2 * T[i][j] - 1
    return D


s_s = time.time()
sp_s = APD(b, n)
e_s = time.time()
print(f'my {e_m - s_m}')
# print(f'di {e - s}')
# print(f'wa {e_w-s_w}')
print(f'sa {e_s - s_s}')
# digekstra.append(e - s)
# my_algorithm.append(e_m - s_m)




pos = nx.spring_layout(G)
# print(nx.clustering(a))
nx.draw(G, pos)
plt.draw()
plt.show()