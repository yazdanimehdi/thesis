import math
import random

import community
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from networkx.algorithms.community.quality import performance
from sklearn.cluster import KMeans

y_time = []
x_time = []
# for n in range(4, 100):
# start = time.time()


class Node:

    def __init__(self,sp, d, v):
        self.x = random.random()*100
        self.y = random.random()*100
        self.d = d
        self.v = v
        self.sp = sp


# G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
G = nx.karate_club_graph()
pos = nx.spring_layout(G)
n = G.number_of_nodes()
# print(nx.clustering(a))
nx.draw(G, pos)
plt.draw()
plt.show()
b = nx.adjacency_matrix(G).toarray()
# print(nx.shortest_path(a, source=0, target=80))
sp_list = np.zeros([n, n])
nodes = list(G.nodes)
start = time.time()
for node_i in range(0, n):
    for node_j in range(node_i, n):
        try:
            sp_list[node_i, node_j] = len(nx.shortest_path(G, source=nodes[node_i], target=nodes[node_j])) - 1
        except nx.exception.NetworkXNoPath:
            sp_list[node_i, node_j] = -1


sp_list = np.maximum(sp_list, sp_list.transpose())
d = list(G.degree)
node_list = [Node(list(map(int, sp_list[i].tolist())), d[i][1], 0) for i in range(0, n)]

x = []
y = []
for item in node_list:
    x.append(item.x)
    y.append(item.y)

types = [i for i in range(1, n)]
for i, type in enumerate(types):
    z = x[i]
    t = y[i]
    plt.scatter(z, t, marker='x', color='red')
    plt.text(z+0.3, t+0.3, type, fontsize=9)
plt.show()
i = 0

t = 20
while i < 5000:
    for item in node_list:
        delta_x = []
        delta_y = []
        for node in node_list:
            if node != item and item.sp[node_list.index(node)] != -1:
                if item.sp[node_list.index(node)] < np.array(sp_list).max()/2:     # 3 is half of maximum SP's + 1
                    delta = math.sqrt((item.x - node.x)**2 + (item.y - node.y)**2)
                    v = max(((1 / (item.sp[node_list.index(node)] ** 7)) * (
                            delta - item.sp[node_list.index(node)] / 6) / (2 * item.d)), 0)
                    a = math.atan((item.y - node.y)/(item.x - node.x))
                    if item.x - node.x > 0:
                        delta_x.append(v * -math.cos(a)*0.01 * t)
                        delta_y.append(v * -math.sin(a) * 0.01 * t)
                    elif item.x - node.x < 0:
                        delta_x.append(v * math.cos(a) * 0.01 * t)
                        delta_y.append(v * math.sin(a) * 0.01 * t)
                    else:
                        delta_x.append(0)
                        if item.y - node.y > 0:
                            delta_y.append(-v * 0.01 * t)
                        elif item.y - node.y < 0:
                            delta_y.append(v * 0.01 * t)
                        else:
                            delta_y.append(0)
                else:
                    delta = math.sqrt((item.x - node.x) ** 2 + (item.y - node.y) ** 2)
                    v = min((delta - item.sp[node_list.index(node)] / 2), 0)  # 2 is power of repulsion
                    a = math.atan((item.y - node.y) / (item.x - node.x))
                    if item.x - node.x > 0:
                        delta_x.append(v * -math.cos(a) * 0.01 * t)
                        delta_y.append(v * -math.sin(a) * 0.01 * t)
                    elif item.x - node.x < 0:
                        delta_x.append(v * math.cos(a) * 0.01 * t)
                        delta_y.append(v * math.sin(a) * 0.01 * t)
                    else:
                        delta_x.append(0)
                        if item.y - node.y > 0:
                            delta_y.append(-v * 0.01 * t)
                        elif item.y - node.y < 0:
                            delta_y.append(v * 0.01 * t)
                        else:
                            delta_y.append(0)
            elif node != item:
                delta = math.sqrt((item.x - node.x)**2 + (item.y - node.y)**2)
                v = min((delta - item.sp[node_list.index(node)]/2), 0)  # 2 is power of repulsion
                a = math.atan((item.y - node.y)/(item.x - node.x))
                if item.x - node.x > 0:
                    delta_x.append(v * -math.cos(a)*0.01)
                    delta_y.append(v * -math.sin(a) * 0.01)
                elif item.x - node.x < 0:
                    delta_x.append(v * math.cos(a) * 0.01)
                    delta_y.append(v * math.sin(a) * 0.01)
                else:
                    delta_x.append(0)
                    if item.y - node.y > 0:
                        delta_y.append(-v * 0.01)
                    elif item.y - node.y < 0:
                        delta_y.append(v * 0.01)
                    else:
                        delta_y.append(0)

        sum_x = np.array(delta_x).sum()
        sum_y = np.array(delta_y).sum()
        item.x += sum_x
        item.y += sum_y
        delta_x.clear()
        delta_y.clear()

    i += 1
#
x = []
y = []
end = time.time()
print(end - start)
for item in node_list:
    x.append(item.x)
    y.append(item.y)

types = [i for i in range(1, n)]
for i, type in enumerate(types):
    z = x[i]
    t = y[i]
    plt.scatter(z, t, marker='x', color='red')
    plt.text(z+0.002, t+0.002, type, fontsize=9)
plt.show()
X = []
for item in x:
    X.append((item, y[x.index(item)]))

y_pred = KMeans(n_clusters=4).fit_predict(X)
nx.draw(G, pos, node_color=y_pred)
plt.draw()
plt.show()

start = time.time()
partition = community.best_partition(G)
end = time.time()
y_kl = []
for item in partition:
    y_kl.append(partition[item])

d = [[] for x in range(len([item for item in set(y_kl)]))]
clusters = [item for item in set(y_kl)]

for item in clusters:
    for node in range(len(y_kl)):
        if y_kl[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
#
print('Kernighan–Lin algorithm', performance(G, sets))
#
d = [[] for x in range(len([item for item in set(y_pred)]))]
clusters = [item for item in set(y_pred)]

for item in clusters:
    for node in range(len(y_pred)):
        if y_pred[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))

print('greedy_modularity_communitie algorithm', performance(G, nx.algorithms.community.greedy_modularity_communities(G)))

print('my algorithm', performance(G, sets))
#
# size = float(len(set(partition.values())))
# pos = nx.spring_layout(G)
# count = 0.
# for com in set(partition.values()):
#     count = count + 1.
#     list_nodes = [nodes for nodes in partition.keys()
#                   if partition[nodes] == com]
#     nx.draw_networkx_nodes(G, pos, list_nodes, node_size=20,
#                            node_color=str(count / size))
#
# nx.draw_networkx_edges(G, pos, alpha=0.5)
# plt.show()
#

# end = time.time()
# y_time.append(end - start)
# x_time.append(n)

# plt.scatter(x_time, y_time)
# plt.show()
