import math
from math import sqrt
import random
from multiprocessing import Pool


import community
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from networkx.algorithms.community.quality import performance
from sklearn.cluster import KMeans
import pandas as pd


class Node:

    def __init__(self, d, ind):
        self.x = random.random()*100
        self.y = random.random()*100
        self.d = d
        self.ind = ind
        self.yes = True


G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
# G = nx.karate_club_graph()
n = G.number_of_nodes()
b = nx.adjacency_matrix(G).toarray()
nodes = list(G.nodes)
s = time.time()
sp_list = pd.read_csv('sp_list.csv').as_matrix()
sp_list = sp_list / sp_list.max()
d = list(G.degree)
node_list = [Node(d[i][1], i) for i in range(0, n)]
i = 0
t = 0
condition_list = np.zeros([n, n])


while i < len(node_list):
    chosen_nodes = np.zeros(n)
    print(i)
    def func(j):
        condition_list = np.zeros(n)
        if 0 < sp_list[i][j] < sp_list.max() / 2:
            condition_value = 1 / (sp_list[i][j] * node_list[i].d)
        else:
            condition_value = -1
        condition_list[j] = condition_value

        return condition_list
    j = range(i + 1, n)
    my_pool = Pool(8)
    condition_list = list(my_pool.map(func, j))
    my_pool.close()
    my_pool.join()
    i += 1

# condition_list = np.maximum(condition_list, condition_list.transpose())
# i = 0
# node_list.sort(key=lambda x: x.d)
# chosen_nodes = np.zeros(n)
# for node in node_list:
#     chosen_node = condition_list[node.ind][:].argmax()
#     chosen_nodes[node.ind] = chosen_node
#
# node_list.sort(key=lambda x: x.ind)
# i = 0
# while i < len(node_list):
#     node_list[i].x = node_list[int(chosen_nodes[i])].x
#     node_list[i].y = node_list[int(chosen_nodes[i])].y
#     i += 1
# e = time.time()
#
# x = []
# y = []
# for item in node_list:
#     x.append(item.x)
#     y.append(item.y)
#
# types = [i for i in range(1, n)]
# for i, type in enumerate(types):
#     z = x[i]
#     t = y[i]
#     plt.scatter(z, t, marker='x', color='red')
#     plt.text(z+0.3, t+0.3, type, fontsize=9)
# plt.show()
#
#
# X = []
# for item in x:
#     X.append((item, y[x.index(item)]))
#
# y_pred = KMeans(n_clusters=30).fit_predict(X)
# pos = nx.spring_layout(G)
# nx.draw(G, pos, node_color=y_pred)
# plt.draw()
# plt.show()
# start = time.time()
# partition = community.best_partition(G)
# end = time.time()
# y_kl = []
# for item in partition:
#     y_kl.append(partition[item])
#
# d = [[] for x in range(len([item for item in set(y_kl)]))]
# clusters = [item for item in set(y_kl)]
#
# for item in clusters:
#     for node in range(len(y_kl)):
#         if y_kl[node] == item:
#             d[clusters.index(item)].append(nodes[node])
# sets = []
# for item in d:
#     sets.append(set(item))
# #
# print('Kernighan–Lin algorithm', performance(G, sets))
# #
# d = [[] for x in range(len([item for item in set(y_pred)]))]
# clusters = [item for item in set(y_pred)]
#
# for item in clusters:
#     for node in range(len(y_pred)):
#         if y_pred[node] == item:
#             d[clusters.index(item)].append(nodes[node])
# sets = []
# for item in d:
#     sets.append(set(item))
#
# print('greedy_modularity_communitie algorithm', performance(G, nx.algorithms.community.greedy_modularity_communities(G)))
#
# print('my algorithm', performance(G, sets))