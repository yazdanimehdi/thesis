import igraph
import matplotlib.pyplot as plt
import numpy as np

import networkx as nx

G = nx.read_weighted_edgelist('1K25.nse', create_using=nx.Graph(), nodetype=int)
n = G.number_of_nodes()
b = nx.adjacency_matrix(G).toarray()
d = [list(G.degree)[i][1] for i in range(len(list(G.degree)))]
b = nx.adjacency_matrix(G).toarray()
P = np.zeros((n, n))
for i in range(len(d)):
    P[:][i] = b[:][i] * 1 / d[i]

p2 = np.matmul(P, P)