import math
from math import sqrt
import random

import community
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from networkx.algorithms.community.quality import performance
from sklearn.cluster import KMeans
import pandas as pd


class Node:

    def __init__(self, container, adjacency_arrey):
        self.container = container
        self.adjacency_matrix = adjacency_arrey


digekstra = []
my_algorithm = []

# n = 400
G = nx.random_tree(500)
# G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
n = G.number_of_nodes()
b = nx.adjacency_matrix(G).toarray()
node_list = [Node([[1, j] for j in np.where(b[i][:] == 1)[0]],  b[i][:]) for i in range(0, n)]
sp_list = np.zeros([n, n])
nodes = list(G.nodes)

# s = time.time()
for node_i in range(0, n):
    for node_j in range(node_i, n):
        try:
            sp_list[node_i, node_j] = len(nx.shortest_path(G, source=nodes[node_i], target=nodes[node_j])) - 1
        except nx.exception.NetworkXNoPath:
            sp_list[node_i, node_j] = -1

sp_list = np.maximum(sp_list, sp_list.transpose())
# e = time.time()
s_m = time.time()
sp = np.zeros([n, n])
for i in range(0, n):
    for item in node_list[i].container:
        for node in node_list[item[1]].container:
            if node[1] != i:
                if node[1] not in [k[1] for k in node_list[i].container]:
                    node_list[i].container.append([item[0] + 1, node[1]])
    for item in node_list[i].container:
        sp[i][item[1]] = item[0]

    node_list[i].container = [[1, j] for j in np.where(b[i][:] == 1)[0]]
e_m = time.time()

def APD(A, N):
    Z = np.matmul(A, A)
    B = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if (A[i][j] == 1 or Z[i][j] > 0) and i != j:
                B[i][j] = 1
    if all(B[i][j] for i in range(n) for j in range(n) if i != j):
        D = 2 * B - A
        return D
    T = APD(B, N)
    X = np.matmul(T, A)
    degree = [sum(A[i][j] for j in range(n)) for i in range(n)]
    D = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if X[i][j] >= T[i][j] * degree[j]:
                D[i][j] = 2 * T[i][j]
            else:
                D[i][j] = 2 * T[i][j] - 1
    return D


s_s = time.time()
sp_s = APD(b, n)
e_s = time.time()
print(f'my {e_m - s_m}')
# print(f'di {e - s}')
# print(f'wa {e_w-s_w}')
print(f'sa {e_s - s_s}')