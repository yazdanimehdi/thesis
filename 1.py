import random
import math
import time

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D, proj3d
import matplotlib.animation as animation
import pandas as pd
from sklearn.cluster import KMeans

y_time = []
x_time = []
# for n in range(4, 100):
# start = time.time()


class Node:

    def __init__(self,sp, d, v):
        self.x = random.random()*1000000
        self.y = random.random()*1000000
        self.d = d
        self.v = v
        self.sp = sp


G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
pos = nx.spring_layout(G)
n = G.number_of_nodes()
# print(nx.clustering(a))
nx.draw(G, pos)
plt.draw()
plt.show()
b = nx.adjacency_matrix(G).toarray()
sp_list = pd.read_csv('sp_list.csv').as_matrix()

d = list(G.degree)
node_list = [Node(list(map(int, sp_list[i].tolist())), d[i][1], 0) for i in range(0, n)]
x = []
y = []
for item in node_list:
    x.append(item.x)
    y.append(item.y)

types = [i for i in range(1, n)]
for i, type in enumerate(types):
    z = x[i]
    t = y[i]
    plt.scatter(z, t, marker='x', color='red')
    plt.text(z+0.3, t+0.3, type, fontsize=9)
plt.show()
i = 0

t = 100
while i < 20:
    print(i)
    for item in node_list:
        delta_x = []
        delta_y = []
        start = time.time()
        for node in node_list:
            if node != item and item.sp[node_list.index(node)] != -1:
                if item.sp[node_list.index(node)] < np.array(sp_list).max()/2:     # 3 is half of maximum SP's + 1
                    delta = math.sqrt((item.x - node.x)**2 + (item.y - node.y)**2)
                    v = max(((1 / (item.sp[node_list.index(node)] ** 7)) * (
                            delta - item.sp[node_list.index(node)] / 6) / (2 * item.d)), 0)
                    a = math.atan((item.y - node.y)/(item.x - node.x))
                    if item.x - node.x > 0:
                        delta_x.append(v * -math.cos(a)*0.01 * t)
                        delta_y.append(v * -math.sin(a) * 0.01 * t)
                    elif item.x - node.x < 0:
                        delta_x.append(v * math.cos(a) * 0.01 * t)
                        delta_y.append(v * math.sin(a) * 0.01 * t)
                    else:
                        delta_x.append(0)
                        if item.y - node.y > 0:
                            delta_y.append(-v * 0.01 * t)
                        elif item.y - node.y < 0:
                            delta_y.append(v * 0.01 * t)
                        else:
                            delta_y.append(0)
                else:
                    delta = math.sqrt((item.x - node.x) ** 2 + (item.y - node.y) ** 2)
                    v = min((delta - item.sp[node_list.index(node)] / 2), 0)  # 2 is power of repulsion
                    a = math.atan((item.y - node.y) / (item.x - node.x))
                    if item.x - node.x > 0:
                        delta_x.append(v * -math.cos(a) * 0.01 * t)
                        delta_y.append(v * -math.sin(a) * 0.01 * t)
                    elif item.x - node.x < 0:
                        delta_x.append(v * math.cos(a) * 0.01 * t)
                        delta_y.append(v * math.sin(a) * 0.01 * t)
                    else:
                        delta_x.append(0)
                        if item.y - node.y > 0:
                            delta_y.append(-v * 0.01 * t)
                        elif item.y - node.y < 0:
                            delta_y.append(v * 0.01 * t)
                        else:
                            delta_y.append(0)
            elif node != item:
                delta = math.sqrt((item.x - node.x)**2 + (item.y - node.y)**2)
                v = min((delta - item.sp[node_list.index(node)]/2), 0)  # 2 is power of repulsion
                a = math.atan((item.y - node.y)/(item.x - node.x))
                if item.x - node.x > 0:
                    delta_x.append(v * -math.cos(a)*0.01)
                    delta_y.append(v * -math.sin(a) * 0.01)
                elif item.x - node.x < 0:
                    delta_x.append(v * math.cos(a) * 0.01)
                    delta_y.append(v * math.sin(a) * 0.01)
                else:
                    delta_x.append(0)
                    if item.y - node.y > 0:
                        delta_y.append(-v * 0.01)
                    elif item.y - node.y < 0:
                        delta_y.append(v * 0.01)
                    else:
                        delta_y.append(0)
        end = time.time()
        print(start - end)

        sum_x = np.array(delta_x).sum()
        sum_y = np.array(delta_y).sum()
        item.x += sum_x
        item.y += sum_y
        delta_x.clear()
        delta_y.clear()
    i += 1
# #
# x = []
# y = []
#
# for item in node_list:
#     x.append(item.x)
#     y.append(item.y)
#
# types = [i for i in range(1, n)]
# for i, type in enumerate(types):
#     z = x[i]
#     t = y[i]
#     plt.scatter(z, t, marker='x', color='red')
#     plt.text(z+0.002, t+0.002, type, fontsize=9)
# plt.show()
# X = []
# for item in x:
#     X.append((item, y[x.index(item)]))
#
# y_pred = KMeans(n_clusters=15).fit_predict(X)
# nx.draw(G, pos, node_color=y_pred)
# plt.draw()
# plt.show()