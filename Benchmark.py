import time
import community
import igraph as ig
from networkx.algorithms import community as cmt
import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from networkx.algorithms.community.quality import performance


# def import_nx_network(net):
#     graph = ig.Graph(n=net.number_of_nodes(), directed=False)
#     graph.add_edges(net.edges())
#
#     return graph
#
#
# def LFR_graph(N, mu1, mu2, tau, min_degree, min_community):
#     net = cmt.LFR_benchmark_graph(N, mu1, mu2, tau, average_degree=min_degree, min_community=min_community, seed=10)
#     graph = import_nx_network(net)
#
#     return graph, net


n = 50
tau1 = 3
tau2 = 1.5
mu = 0.4
min_deg = 25
min_com = 50
# G = nx.barbell_graph(40, 0)
# g, G = LFR_graph(n, tau1, tau2, mu, min_deg, min_com)
import random

G = nx.read_weighted_edgelist('1K25.nse', create_using=nx.Graph(), nodetype=int)


class Node:
    def __init__(self, d, ind):
        self.x = random.random()*100
        self.y = random.random()*100
        self.d = d
        self.ind = ind


# n = 80
# G = nx.barbell_graph(600, 0)
n = G.number_of_nodes()
pos = nx.spring_layout(G)
nx.draw(G, pos)
plt.draw()
plt.show()
b = nx.adjacency_matrix(G).toarray()
nodes = list(G.nodes)


def APD(A, N):
    Z = np.matmul(A, A)
    B = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if (A[i][j] == 1 or Z[i][j] > 0) and i != j:
                B[i][j] = 1
    if all(B[i][j] for i in range(n) for j in range(n) if i != j):
        D = 2 * B - A
        return D
    T = APD(B, N)
    X = np.matmul(T, A)
    degree = [sum(A[i][j] for j in range(n)) for i in range(n)]
    D = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if X[i][j] >= T[i][j] * degree[j]:
                D[i][j] = 2 * T[i][j]
            else:
                D[i][j] = 2 * T[i][j] - 1
    return D


s = time.time()
sp_list = APD(b, n)
d = list(G.degree)
node_list = [Node(d[i][1], i) for i in range(0, n)]
sp_list_new = sp_list.copy()
sp_list_new[sp_list_new > sp_list_new.max()/2] = -1
sp_list_new[sp_list_new == 0] = -1
a = [list(G.degree)[i][1] for i in range(len(list(G.degree)))]
P = np.zeros((n, n))
for i in range(len(a)):
    P[:][i] = b[:][i] * (1 / a[i])
p2 = np.matmul(P, P)
p4 = np.matmul(p2, p2)
condition_list_new = (1 / (sp_list_new * [d[i][1] for i in range(0, n)])).transpose()
condition_list_new[condition_list_new < 0] = 0
condition_list_new = np.triu(condition_list_new, 0)
condition_list_new = np.maximum(condition_list_new, condition_list_new.transpose())


node_list.sort(key=lambda x: x.d)
chosen_nodes = np.zeros(n)
for node in node_list:
    chosen_node = np.argwhere(condition_list_new[node.ind][:] == np.amax(condition_list_new[node.ind][:]))
    if chosen_node.__len__() == 1:
        chosen_nodes[node.ind] = chosen_node[0]
    else:
        min_p = 0
        chosen = 0
        for c in chosen_node:
            if p2[node.ind][c] >= min_p:
                min_p = p2[node.ind][c]
                chosen = c
        chosen_nodes[node.ind] = chosen

node_list.sort(key=lambda x: x.ind)
i = 0
while i < len(node_list):
    node_list[i].x = node_list[int(chosen_nodes[i])].x
    node_list[i].y = node_list[int(chosen_nodes[i])].y
    i += 1
e = time.time()

x = []
y = []
for item in node_list:
    x.append(item.x)
    y.append(item.y)

# types = [i for i in range(1, n)]
# for i, type in enumerate(types):
#     z = x[i]
#     t = y[i]
#     plt.scatter(z, t, marker='x', color='red')
#     plt.text(z+0.3, t+0.3, type, fontsize=9)
# plt.show()

X = []
for item in x:
    X.append((item, y[x.index(item)]))

y_pred = KMeans(n_clusters=n).fit_predict(X)
# nx.draw(G, pos, node_color=y_pred)
# plt.draw()
# plt.show()

d = [[] for x in range(len([item for item in set(y_pred)]))]
clusters = [item for item in set(y_pred)]
for item in clusters:
    for node in range(len(y_pred)):
        if y_pred[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
print('my algorithm', performance(G, sets))
print('my algorithm Time', e - s, '\n')

start = time.time()
partition = community.best_partition(G)
end = time.time()
y_kl = []
for item in partition:
    y_kl.append(partition[item])

d = [[] for x in range(len([item for item in set(y_kl)]))]
clusters = [item for item in set(y_kl)]

for item in clusters:
    for node in range(len(y_kl)):
        if y_kl[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
#
print('Kernighan–Lin algorithm', performance(G, sets))
print('Kernighan–Lin algorithm Time', end - start, '\n')
#

print('greedy_modularity_community algorithm', performance(G, nx.algorithms.community.greedy_modularity_communities(G)))
st = time.time()
nx.algorithms.community.greedy_modularity_communities(G)
en = time.time()
print('greedy_modularity_community algorithm Time', en - st, '\n')

# memberships = []
# times = []
# print('1')
# # start_1 = time.time()
# # memberships.append(g.community_edge_betweenness().as_clustering().membership)
# # end_1 = time.time()
# # times.append(end_1-start_1)
# start_2 = time.time()
# print('2')
# memberships.append(g.community_walktrap().as_clustering().membership)
# end_2 = time.time()
# times.append(end_2-start_2)
# start_3 = time.time()
# print('3')
# memberships.append(g.community_fastgreedy().as_clustering().membership)
# end_3 = time.time()
# times.append(end_3-start_3)
# start_4 = time.time()
# print('4')
# memberships.append(g.community_label_propagation().membership)
# end_4 = time.time()
# times.append(end_4-start_4)
# # start_5 = time.time()
# # memberships.append(g.community_optimal_modularity().as_clustering().membership)
# # end_5 = time.time()
# # times.append(end_5-start_5)
# start_6 = time.time()
# print('5')
# memberships.append(g.community_infomap().membership)
# end_6 = time.time()
# times.append(end_6-start_6)
# start_7 = time.time()
# print('6')
# memberships.append(g.community_leading_eigenvector().membership)
# end_7 = time.time()
# times.append(end_7-start_7)
# # start_8 = time.time()
# # memberships.append(g.community_leading_eigenvector_naive().membership)
# # end_8 = time.time()
# # times.append(end_8-start_8)
# print('7')
# start_9 = time.time()
# memberships.append(g.community_multilevel().membership)
# end_9 = time.time()
# times.append(end_9-start_9)
# start_10 = time.time()
# memberships.append(g.community_spinglass().membership)
# end_10 = time.time()
# times.append(end_10-start_10)


# def set_gen(com_list):
#     d = [[] for x in range(len([item for item in set(com_list)]))]
#     clusters = [item for item in set(com_list)]
#
#     for item in clusters:
#         for node in range(len(com_list)):
#             if com_list[node] == item:
#                 d[clusters.index(item)].append(nodes[node])
#     sets = []
#     for item in d:
#         sets.append(set(item))
#
#     return sets
#
#
# performances = []
#
# for item in memberships:
#     performances.append(performance(G, set_gen(item)))
#
#
# # print('community_edge_betweenness', performances[0])
# # print('community_edge_betweenness_time', times[0], '\n')
#
# print('community_walktrap', performances[0])
# print('community_walktrap_time', times[0], '\n')
#
# print('community_fastgreedy', performances[1])
# print('community_fastgreedy_time', times[1], '\n')
#
# print('community_label_propagation', performances[2])
# print('community_label_propagation_time', times[2], '\n')
#
# # print('community_optimal_modularity', performances[4])
# # print('community_optimal_modularity_time', times[4], '\n')
#
# print('community_infomap', performances[3])
# print('community_infomap_time', times[3], '\n')
#
# print('community_leading_eigenvector', performances[4])
# print('community_leading_eigenvector_time', times[4], '\n')
#
# # print('community_leading_eigenvector_naive', performances[6])
# # print('community_leading_eigenvector_naive_time', times[6], '\n')
#
# print('community_multilevel', performances[5])
# print('community_multilevel_time', times[5], '\n')
#
# print('community_spinglass', performances[7])
# print('community_spinglass_time', times[7], '\n')

text = '''3 42 169 227 232 238 285 301 311 533 777 
33 34 37 62 108 183 184 236 242 345 639 
25 76 79 155 179 270 274 287 298 353 668 
43 80 82 201 277 279 370 386 393 418 467 497 508 509 556 561 660 686 691 704 870 
28 78 97 213 218 228 380 401 460 465 494 527 530 562 573 581 635 655 693 719 728 936 
23 31 49 60 73 74 83 85 94 114 115 121 123 124 125 129 133 137 145 152 172 176 191 192 198 212 220 223 225 229 231 249 252 253 265 276 289 303 330 334 355 358 368 369 384 387 391 392 412 419 422 430 438 441 444 447 448 453 454 457 466 471 505 521 523 533 535 545 550 560 565 579 591 597 607 612 613 614 628 630 649 661 663 668 672 674 695 700 707 711 712 717 721 724 767 779 780 786 795 799 800 804 806 817 821 823 854 859 861 866 870 878 883 895 897 912 918 919 920 922 923 924 930 936 937 952 966 971 992 994 
1 5 17 210 211 270 292 296 474 493 495 501 511 555 572 574 589 610 617 643 652 658 675 676 679 688 706 713 723 726 733 739 743 745 748 763 772 782 788 790 809 828 851 879 885 913 933 942 948 967 990 
10 66 102 131 206 217 219 239 259 796 
29 50 53 90 104 118 136 141 144 150 156 160 165 166 174 205 214 215 254 256 264 278 290 293 297 307 310 320 336 356 359 366 371 403 411 416 469 479 482 499 509 521 525 537 549 551 575 601 606 608 618 620 640 651 653 681 685 720 735 742 749 757 758 762 765 768 769 776 807 812 814 865 901 906 931 940 949 953 958 964 973 976 993 997 998 
9 86 120 182 189 302 306 318 326 339 346 376 407 449 481 544 605 608 771 828 
21 27 72 103 168 170 197 229 240 251 379 415 417 425 445 496 599 618 644 684 
22 26 138 142 312 328 340 378 405 421 544 639 999 
30 67 100 119 209 226 237 283 291 390 
4 8 11 58 95 135 163 202 203 248 
39 91 134 185 308 319 337 372 382 385 402 406 428 538 577 629 860 899 
19 35 36 51 92 101 113 143 149 150 154 171 180 200 208 233 244 268 271 280 288 314 335 364 365 437 443 451 454 455 470 472 475 478 484 485 513 514 516 517 536 557 582 585 602 616 625 636 641 645 647 659 671 683 708 710 721 729 731 738 746 753 761 770 773 777 792 797 808 817 820 822 825 826 831 835 837 841 846 847 852 853 867 882 888 900 905 911 917 926 938 946 957 968 970 981 988 1000 
13 14 15 20 24 27 31 40 46 63 75 77 81 84 88 99 105 116 127 137 140 142 146 148 159 173 186 190 193 216 222 230 235 245 246 247 252 263 273 300 315 321 323 327 329 332 333 334 338 341 343 344 347 352 373 377 388 389 396 426 429 440 452 456 459 461 468 473 476 490 492 498 500 503 510 516 525 528 529 531 534 547 558 559 563 564 566 569 571 576 578 586 590 592 596 598 600 615 622 626 627 634 645 650 654 656 665 690 692 696 699 703 714 716 730 736 740 744 751 754 758 766 771 775 778 783 790 793 796 797 798 801 803 811 813 815 818 829 836 838 839 845 849 855 857 863 869 877 881 887 889 890 903 906 908 916 927 932 934 939 944 945 951 954 958 959 960 961 963 972 974 978 979 983 986 996 999 
16 38 47 61 71 89 93 96 98 109 110 117 122 153 187 194 195 207 211 243 250 258 260 266 267 275 281 282 295 305 309 316 317 324 330 349 350 355 367 398 399 408 409 413 414 423 435 445 446 464 478 487 488 502 507 518 519 526 539 540 541 543 603 609 619 621 632 646 664 670 673 678 680 682 687 701 709 716 718 727 764 774 787 789 810 816 840 842 844 848 850 862 868 873 874 898 899 904 916 925 926 931 941 947 975 989 
7 39 56 87 98 112 175 177 360 375 394 439 462 480 486 860 
2 6 12 32 41 43 45 55 65 68 78 128 130 132 147 151 158 161 162 167 178 180 181 188 196 199 207 221 224 234 241 257 262 269 272 278 294 299 304 313 322 325 331 342 348 351 354 357 361 381 395 397 400 420 427 433 434 436 442 450 458 459 463 477 483 489 491 512 514 515 522 542 546 552 554 567 570 580 582 584 587 588 593 594 595 604 609 611 631 633 637 638 642 648 650 657 662 666 697 698 702 705 715 722 725 732 734 737 741 750 755 759 760 776 779 781 784 785 791 794 802 804 805 824 830 832 834 843 864 869 871 875 876 880 884 886 892 893 896 907 909 910 914 915 928 929 933 943 950 955 956 961 969 977 980 982 984 985 987 990 991 995 
18 48 52 57 59 63 69 85 106 111 164 255 261 267 284 286 362 363 365 368 374 383 390 404 410 424 431 432 504 506 520 524 532 548 553 568 583 623 624 667 669 677 689 694 747 752 756 819 827 833 835 856 858 865 872 891 894 902 921 935 960 962 965 
44 54 64 70 107 126 139 157 204 420 
'''
ground_truth = np.zeros(G.number_of_nodes)
real = []
for item in clusters:
    real.append(item.split()[:-1])

i = 1
for item in real:
    for node in item:
        ground_truth[int(node)] = i
    i +=1

truth = ig.Clustering(ground_truth)
ig.compare_communities(truth, ig.Clustering(y_pred), method='nmi')
ig.compare_communities(truth, ig.Clustering(y_kl), method='nmi')