
from __future__ import print_function, division  # Required for stderr output, must be the first import
import sys
import os  # Pathes processing
#import igraph as ig
import random as rand
try:
    # ATTENTION: Python3 newer treats imports as realtive and results in error here unlike Python2
    from utils.parser_nsl import asymnet, loadNsl  #pylint: disable=E0611,E0401
except ImportError:
    # Note: this case should be the second because explicit relative imports cause various errors
    # under Python2 and Python3, which complicates thier handling
    from .utils.parser_nsl import asymnet, loadNsl  #pylint: disable=E0611,E0401

# Default number of the resulting clusterings (partitions, i.e files that contain disjoint clusters)
_RESNUM = 1


class Params(object):
    """Input parameters (arguments)"""
    def __init__(self):
        """Parameters:
        groundtruth  - flile name of the ground truth clustering
        network  - flile name of the input network
        dirnet  - whether the input network is directed
        outnum  - number of the resulting clusterings
        randseed  - seed for the clustering generation (automatically generated if not specified)
        outpseed  - whether to output the seed (automatically set to True on if the seed is generated automatically)
        outdir  - output directory
        outname  - base name of the output file based on the network name
        outext  - extenstion of the output files based on the groundtruth extension
        """
        self.groundtruth = None
        self.network = None
        self.dirnet = False
        self.outnum = _RESNUM
        self.randseed = None
        self.outpseed = False
        self.outdir = None
        self.outname = None
        self.outext = ''


def parseParams(args):
    """Parse user-specified parameters

    returns  - parsed input arguments, Params()
    """
    assert isinstance(args, (tuple, list)) and args, 'Input arguments must be specified'
    prm = Params()

    for arg in args:
        # Validate input format
        preflen = 3
        if arg[0] != '-' or len(arg) <= preflen:
            raise ValueError('Unexpected argument: ' + arg)

        if arg[1] == 'g':
            prm.groundtruth = arg[preflen:]
            prm.outext = os.path.splitext(prm.groundtruth)[1]
        elif arg[1] == 'i':
            pos = arg.find('=', 2)
            if pos == -1 or arg[2] not in 'ud=' or len(arg) == pos + 1:
                raise ValueError('Unexpected argument: ' + arg)
            pos += 1
            prm.network = arg[pos:]
            prm.outname, netext = os.path.splitext(os.path.split(prm.network)[1])
            prm.dirnet = asymnet(netext.lower(), arg[2] == 'd')
            if not prm.outname:
                raise ValueError('Invalid network name (is a directory): ' + prm.network)
        elif arg[1] == 'n':
            prm.outnum = int(arg[preflen:])
            assert prm.outnum >= 1, 'outnum must be a natural number'
        elif arg[1] == 'r':
            prm.randseed = arg[preflen:]
        elif arg[1] == 'o':
            prm.outdir = arg[preflen:]
        else:
            raise ValueError('Unexpected argument: ' + arg)

    if not (prm.groundtruth and prm.network):
        raise ValueError('Input network and groundtruth file names must be specified')
    if not prm.outdir:
        prm.outdir = os.path.split(prm.network)[0]
        if not prm.outdir:
            prm.outdir = '.'
    if not prm.randseed:
        try:
            prm.randseed = ''.join(str(ord(c)) for c in os.urandom(8))
        except NotImplementedError:
            prm.randseed = str(rand.random())
        prm.outpseed = True

    return prm


def MyAlgorithm(*args):
    import networkx as nx
    import numpy as np
    from sklearn.cluster import KMeans
    import random
    """Generate random clusterings for the specified network"""
    prm = parseParams(args)
    print('Starting MyAlgorithm clustering:'
          '\n\tgroundtruth: {}'
          '\n\t{} network: {}'
          '\n\t{} cls of {} in {} with randseed: {}'
          .format(prm.groundtruth, 'directed' if prm.dirnet else 'undirected', prm.network
                  , prm.outnum, prm.outname + prm.outext, prm.outdir, prm.randseed))
    # Load Data from simple real-world networks
    G = nx.read_weighted_edgelist(prm.network, create_using=nx.Graph(), nodetype=int)
    # Load statistics from the ground thruth
    groundstat = []
    with open(prm.groundtruth, 'r') as fground:
        for line in fground:
            # Skip empty lines and comments (possible header)
            if not line or line[0] == '#':
                continue
            groundstat.append(len(line.split()))

    # Create outpdir if required
    if prm.outdir and not os.path.exists(prm.outdir):
        os.makedirs(prm.outdir)
    # Geneate rand clsuterings
        class Node:
            def __init__(self, d, ind):
                self.x = random.random() * 100
                self.y = random.random() * 100
                self.d = d
                self.ind = ind

        n = G.number_of_nodes()
        pos = nx.spring_layout(G)
        b = nx.adjacency_matrix(G).toarray()
        nodes = list(G.nodes)

        def APD(A, N):
            Z = np.matmul(A, A)
            B = np.zeros([N, N])
            for i in range(N):
                for j in range(N):
                    if (A[i][j] == 1 or Z[i][j] > 0) and i != j:
                        B[i][j] = 1
            if all(B[i][j] for i in range(n) for j in range(n) if i != j):
                D = 2 * B - A
                return D
            T = APD(B, N)
            X = np.matmul(T, A)
            degree = [sum(A[i][j] for j in range(n)) for i in range(n)]
            D = np.zeros([N, N])
            for i in range(N):
                for j in range(N):
                    if X[i][j] >= T[i][j] * degree[j]:
                        D[i][j] = 2 * T[i][j]
                    else:
                        D[i][j] = 2 * T[i][j] - 1
            return D
        sp_list = APD(b, n)
        d = list(G.degree)
        node_list = [Node(d[i][1], i) for i in range(0, n)]
        sp_list_new = sp_list.copy()
        sp_list_new[sp_list_new > sp_list_new.max() / 2] = -1
        sp_list_new[sp_list_new == 0] = -1
        a = [list(G.degree)[i][1] for i in range(len(list(G.degree)))]
        P = np.zeros((n, n))
        for i in range(len(a)):
            P[:][i] = b[:][i] * (1 / a[i])
        p2 = np.matmul(P, P)
        p4 = np.matmul(p2, p2)
        condition_list_new = (1 / (sp_list_new * [d[i][1] for i in range(0, n)])).transpose()
        condition_list_new[condition_list_new < 0] = 0
        condition_list_new = np.triu(condition_list_new, 0)
        condition_list_new = np.maximum(condition_list_new, condition_list_new.transpose())

        node_list.sort(key=lambda x: x.d)
        chosen_nodes = np.zeros(n)
        for node in node_list:
            chosen_node = np.argwhere(condition_list_new[node.ind][:] == np.amax(condition_list_new[node.ind][:]))
            if chosen_node.__len__() == 1:
                chosen_nodes[node.ind] = chosen_node[0]
            else:
                min_p = 0
                chosen = 0
                for c in chosen_node:
                    if p2[node.ind][c] >= min_p:
                        min_p = p2[node.ind][c]
                        chosen = c
                chosen_nodes[node.ind] = chosen

        node_list.sort(key=lambda x: x.ind)
        i = 0
        while i < len(node_list):
            node_list[i].x = node_list[int(chosen_nodes[i])].x
            node_list[i].y = node_list[int(chosen_nodes[i])].y
            i += 1

        x = []
        y = []
        for item in node_list:
            x.append(item.x)
            y.append(item.y)
        X = []
        for item in x:
            X.append((item, y[x.index(item)]))

        y_pred = KMeans(n_clusters=n).fit_predict(X)

        d = [[] for x in range(len([item for item in set(y_pred)]))]
        clusters = [item for item in set(y_pred)]
        for item in clusters:
            for node in range(len(y_pred)):
                if y_pred[node] == item:
                    d[clusters.index(item)].append(nodes[node])

        # Output resulting clusters
        with open('/'.join((prm.outdir, ''.join((prm.outname, '_', str(prm.outnum), prm.outext)))), 'w') as fout:
            for cl in d:
                # Note: print() unlike fout.write() appends the newline
                print(' '.join(cl), file=fout)

    print('Random clusterings are successfully generated')


if __name__ == '__main__':
    if len(sys.argv) > 2:
        MyAlgorithm(*sys.argv[1:])
    else:
        print('\n'.join(('Produces random disjoint partitioning (clusters are formed with rand nodes and their neighbors)'
                         ' for the input network specified in the NSL format (generalizaiton of NCOL, SNAP, etc.)\n',
                         'Usage: {app} -g=<ground_truth> -i[{{u, d}}]=<input_network> [-n=<res_num>] [-r=<rand_seed>] [-o=<outp_dir>]',
                         '',
                         '  -g=<ground_truth>  - ground truth clustering as a template for sizes of the resulting communities',
                         '  -i[X]=<input_network>  - file of the input network in the format: <src_id> <dst_id> [<weight>]',
                         '    Xu  - undirected input network (<src_id> <dst_id> implies also <dst_id> <src_id>). Default',
                         '    Xd  - directed input network (both <src_id> <dst_id> and <dst_id> <src_id> are specified)',
                         '    NOTE: (un)directed flag is considered only for the networks with non-NSL file extension',
                         '  -n=<res_num>  - number of the resulting clusterings to generate. Default: {resnum}',
                         '  -r=<rand_seed>  - random seed, string. Default: value from the system rand source (otherwise current time)',
                         '  -o=<output_communities>  - . Default: ./<input_network>/'
                         )).format(app=sys.argv[0], resnum=_RESNUM))
