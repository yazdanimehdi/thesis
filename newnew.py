import math
from math import sqrt
import random

import community
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from networkx.algorithms.community.quality import performance
from sklearn.cluster import KMeans
import pandas as pd


# G = nx.connected_caveman_graph(20, 10)
# G = nx.convert_node_labels_to_integers(G)
# G = nx.read_weighted_edgelist('network.dat', create_using=nx.Graph(), nodetype=int)
# G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
n = G.number_of_nodes()
# pos = nx.spring_layout(G)
# nx.draw(G, pos)
# plt.draw()
# plt.show()
b = nx.adjacency_matrix(G).toarray()
nodes = list(G.nodes)


def APD(A, N):
    Z = np.matmul(A, A)
    B = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if (A[i][j] == 1 or Z[i][j] > 0) and i != j:
                B[i][j] = 1
    if all(B[i][j] for i in range(n) for j in range(n) if i != j):
        D = 2 * B - A
        return D
    T = APD(B, N)
    X = np.matmul(T, A)
    degree = [sum(A[i][j] for j in range(n)) for i in range(n)]
    D = np.zeros([N, N])
    for i in range(N):
        for j in range(N):
            if X[i][j] >= T[i][j] * degree[j]:
                D[i][j] = 2 * T[i][j]
            else:
                D[i][j] = 2 * T[i][j] - 1
    return D


sp_list = pd.read_csv('sp_list.csv').as_matrix()
d = list(G.degree)

# sp_list = APD(b, n)
s = time.time()
node_list = [Node(d[i][1], i) for i in range(0, n)]
sp_list_new = sp_list.copy()
sp_list_new[sp_list_new > sp_list_new.max()/2] = -10000000
sp_list_new[sp_list_new == 0] = -1
a = [list(G.degree)[i][1] for i in range(len(list(G.degree)))]
P = np.zeros((n, n))
for i in range(len(a)):
    P[:][i] = b[:][i] * (1 / a[i])

p2 = np.matmul(P, P)

condition_list_new = (-sp_list_new/p2)
condition_list_new[condition_list_new >= 0] = -1000000
# condition_list_new = np.triu(condition_list_new, 0)
# condition_list_new = np.maximum(condition_list_new, condition_list_new.transpose())

# p4 = np.matmul(p2, p2)

# node_list.sort(key=lambda x: x.d)
chosen_nodes = np.zeros(n)
for node in node_list:
    chosen_node = np.argwhere(condition_list_new[node.ind][:] == np.amax(condition_list_new[node.ind][:]))
    chosen_nodes[node.ind] = chosen_node[0]


node_list.sort(key=lambda x: x.ind)
comm = []
i = 0
while i < len(node_list):
    comm.append(node_list[int(chosen_nodes[i])].ind)
    i += 1


comm_arr = np.array(comm)
comm_arr_comp = [ np.argwhere(comm_arr == i) for i in range(node_list.__len__())]
my_list = dict()
i = 0
for item in comm_arr_comp:
    if item.size != 0:
        my_list[i] = item
    i += 1

secondary = dict()
main = list(my_list.keys())
for item in main:
    for cluster in my_list:
        for node in my_list[cluster]:
            if item == node[0]:
                if cluster not in secondary:
                    secondary[cluster] = np.concatenate((my_list[item], my_list[cluster]), axis=0)
                else:
                    secondary[cluster] = np.concatenate((my_list[item], secondary[cluster]), axis=0)
                if cluster in main:
                    main.remove(cluster)


# third = dict()
# main = list(secondary.keys())
# for item in main:
#     flag = False
#     for cluster in secondary:
#         for node in secondary[cluster]:
#             if item == node:
#                 third[cluster] = np.concatenate((secondary[item], secondary[cluster]), axis=0)
#                 main.remove(item)
#                 flag = True
#     if flag is False:
#         third[item] = secondary[item]

e = time.time()
d = [[] for x in range(secondary.__len__())]
i = 0
for item in secondary:
    d[i].append(item)
    for node in secondary[item]:
        d[i].append(node[0])
    i += 1


sets = []
for item in d:
    sets.append(set(item))

# x = []
# y = []
# for item in node_list:
#     x.append(item.x)
#     y.append(item.y)
#
# # types = [i for i in range(1, n)]
# # for i, type in enumerate(types):
# #     z = x[i]
# #     t = y[i]
# #     plt.scatter(z, t, marker='x', color='red')
# #     plt.text(z+0.3, t+0.3, type, fontsize=9)
# # plt.show()
#
# X = []
# for item in x:
#     X.append((item, y[x.index(item)]))
#
# y_pred = KMeans(n_clusters=n).fit_predict(X)
# nx.draw(G, pos, node_color=y_pred)
# plt.draw()
# plt.show()
#
# d = [[] for x in range(len([item for item in set(y_pred)]))]
# clusters = [item for item in set(y_pred)]
# for item in clusters:
#     for node in range(len(y_pred)):
#         if y_pred[node] == item:
#             d[clusters.index(item)].append(nodes[node])
# sets = []
# for item in d:
#     sets.append(set(item))
print('my algorithm', performance(G, sets))
print('my algorithm Time', e - s, '\n')
#
# #
start = time.time()
partition = community.best_partition(G)
end = time.time()
y_kl = []
for item in partition:
    y_kl.append(partition[item])

d = [[] for x in range(len([item for item in set(y_kl)]))]
clusters = [item for item in set(y_kl)]

for item in clusters:
    for node in range(len(y_kl)):
        if y_kl[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
#
print('Kernighan–Lin algorithm', performance(G, d))
print('Kernighan–Lin algorithm Time', end - start, '\n')


# print('greedy_modularity_community algorithm', performance(G, nx.algorithms.community.greedy_modularity_communities(G)))
# st = time.time()
# nx.algorithms.community.greedy_modularity_communities(G)
# en = time.time()
# print('greedy_modularity_community algorithm Time', en - st, '\n')
#
