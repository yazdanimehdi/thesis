import math
import random
import time
from multiprocessing import Pool
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt
import pandas as pd
from sklearn.cluster import KMeans
from networkx.algorithms.community.quality import performance
import community


class Node:

    def __init__(self, indicator, degree, shortest_path, delta_x, delta_y, x, y):
        self.indicator = indicator
        self.degree = degree
        self.shortest_path = shortest_path
        self.delta_x = delta_x
        self.delta_y = delta_y
        self.x = x
        self.y = y


# G = nx.read_edgelist('facebook_combined.txt', create_using=nx.Graph(), nodetype=int)
G = nx.davis_southern_women_graph()
pos = nx.spring_layout(G)
n = G.number_of_nodes()
# print(nx.clustering(a))
nx.draw(G, pos)
plt.draw()
plt.show()
b = nx.adjacency_matrix(G).toarray()
d = list(G.degree)
# sp_list = pd.read_csv('sp_list.csv').as_matrix()
sp_list = np.zeros([n, n])
nodes = list(G.nodes)
for node_i in range(0, n):
    for node_j in range(node_i, n):
        try:
            sp_list[node_i, node_j] = len(nx.shortest_path(G, source=nodes[node_i], target=nodes[node_j])) - 1
        except nx.exception.NetworkXNoPath:
            sp_list[node_i, node_j] = -1


sp_list = np.maximum(sp_list, sp_list.transpose())
x = np.random.rand(n)*100
y = np.random.rand(n)*100
x_old = x.copy()
y_old = y.copy()

types = [i for i in range(1, n)]
for i, type in enumerate(types):
    z = x[i]
    t = y[i]
    plt.scatter(z, t, marker='x', color='red')
    plt.text(z+0.3, t+0.3, type, fontsize=9)
plt.show()


i = 0

node_list = [Node(i, d[i][1], list(map(int, sp_list[i].tolist())), 0, 0, x, y) for i in range(0, n)]


# distance_mat = np.zeros([n, n])
# while i < n:
#     j = i
#     while j < n:
#         distance_mat[i][j] = sqrt((x[i] - x[j]) ** 2 + (y[i] - y[j]) ** 2)
#         j += 1
#     i += 1
# distance = np.maximum(distance_mat, distance_mat.transpose())

def my_algorithm(node):
    t = 100
    delta_x = []
    delta_y = []
    i = 0
    # print(node.x == x_old)
    while i < len(node.shortest_path):
        if node.shortest_path[i] != 0:
            delta = sqrt((node.x[node.indicator] - node.x[i]) ** 2 + (node.y[node.indicator] - node.y[i]) ** 2)
            a = math.atan((node.y[node.indicator] - node.y[i]) / (node.x[node.indicator] - node.x[i]))
            if node.shortest_path[i] != -1:
                if node.shortest_path[i] < np.array(sp_list).max() / 2:
                    v = max(((1 / (node.shortest_path[i] ** 7)) * (
                            delta - node.shortest_path[i] / 6) / (2 * node.degree)), 0)
                    if node.x[node.indicator] - node.x[i] > 0:
                        delta_x.append(v * -math.cos(a) * 0.01 * t)
                        delta_y.append(v * -math.sin(a) * 0.01 * t)
                    elif node.x[node.indicator] - node.x[i] < 0:
                        delta_x.append(v * math.cos(a) * 0.01 * t)
                        delta_y.append(v * math.sin(a) * 0.01 * t)
                    else:
                        delta_x.append(0)
                        if node.y[node.indicator] - node.y[i] > 0:
                            delta_y.append(-v * 0.01 * t)
                        elif node.y[node.indicator] - node.y[i] < 0:
                            delta_y.append(v * 0.01 * t)
                        else:
                            delta_y.append(0)
            elif node.shortest_path[i] == -1 or (node.shortest_path[i] != -1 and node.shortest_path[i] > np.array(sp_list).max() / 2):
                v = min((delta - node.shortest_path[i] / 2), 0)  # 2 is power of repulsion
                if node.x[node.indicator] - node.x[i] > 0:
                    delta_x.append(v * -math.cos(a) * 0.01 * t)
                    delta_y.append(v * -math.sin(a) * 0.01 * t)
                elif node.x[node.indicator] - node.x[i] < 0:
                    delta_x.append(v * math.cos(a) * 0.01 * t)
                    delta_y.append(v * math.sin(a) * 0.01 * t)
                else:
                    delta_x.append(0)
                    if node.y[node.indicator] - node.y[i] > 0:
                        delta_y.append(-v * 0.01 * t)
                    elif node.y[node.indicator] - node.y[i] < 0:
                        delta_y.append(v * 0.01 * t)
                    else:
                        delta_y.append(0)
        i += 1

    sum_x = np.array(delta_x).sum()
    sum_y = np.array(delta_y).sum()
    node.delta_x = sum_x
    node.delta_y = sum_y
    return node


my_pool = Pool(16)
j = 0

start = time.time()
while j < 300:
    for item in node_list:
        x[item.indicator] += item.delta_x
        y[item.indicator] += item.delta_y
    for item in node_list:
        item.x = x
        item.y = y
    node_list = list(my_pool.map(my_algorithm, node_list))
    j += 1
end = time.time()
my_pool.close()
my_pool.join()
print(end-start)

types = [i for i in range(1, n)]
for i, type in enumerate(types):
    z = x[i]
    t = y[i]
    plt.scatter(z, t, marker='x', color='red')
    plt.text(z+0.002, t+0.002, type, fontsize=9)
plt.show()

X = []
for item in x:
    X.append((item, y[x.tolist().index(item)]))

y_pred = KMeans(n_clusters=5).fit_predict(X)
nx.draw(G, pos, node_color=y_pred)
plt.draw()
plt.show()

d = [[] for x in range(len([item for item in set(y_pred)]))]
clusters = [item for item in set(y_pred)]

for item in clusters:
    for node in range(len(y_pred)):
        if y_pred[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))

print('greedy_modularity_communitie algorithm', performance(G, nx.algorithms.community.greedy_modularity_communities(G)))

print('my algorithm', performance(G, sets))

start = time.time()
partition = community.best_partition(G)
end = time.time()
y_kl = []
for item in partition:
    y_kl.append(partition[item])

d = [[] for x in range(len([item for item in set(y_kl)]))]
clusters = [item for item in set(y_kl)]

for item in clusters:
    for node in range(len(y_kl)):
        if y_kl[node] == item:
            d[clusters.index(item)].append(nodes[node])
sets = []
for item in d:
    sets.append(set(item))
#
print('Kernighan–Lin algorithm', performance(G, sets))
#
print(end - start)



